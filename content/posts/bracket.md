+++
date        = "2020-09-26"
title       = "The Bracket"
description = "The square bracket is an unflappable being in the world of punctuation."
tags        = [ "punctuation" ]
topics      = [ "punctuation" ]
+++

The square bracket is an unflappable being in the world of punctuation.

Let's do some real talk here for a sec. Drop the fiction.

Square brackets are my favorite. Firstly – come on, nesting within parentheses (here's an example [but you probably knew this already (sorry)])? Two of my favorite things, when combined, producing a third? Yes PLEASE let's do it like that.

Secondly, square brackets bring additional fun(?[!]) by adding another layer. What you're reading is also being processed by some unseen, unknown, possibly unreliable being [definitely reliable though] who might want to add their own "metatextual" "opinion" regardless of how excellently the original text was written [what? no. disregard that].

Thirdly, in some programming languages, items in square brackets represent lists. For example:
```python
synonyms_for_squint = ["squinch", "look askance", "cross one's eyes as if in strabismus"]
```
And who doesn't love lists? Panthers, that's who. Listless, those panthers are. But stealthy. Still so stealthy.

Are we still going for some fictional thing here? Fine:

Square brackets were introduced in The Future by Viscount Casablanca (you'll know who I'm talking about later<sup>[citation needed]</sup>), who made a deal with The Past to make square brackets timeless, at the cost of Viscount Casablanca's speech. From then on, reports from The Future document Viscount Casablanca's dialogue as simply “[gestures wildly]”. Viscount Casablanca was, is, and will be okay with that.

<sub><em>This article is part of a series on [**punctuation**](/tags/punctuation). It's pretty much all made up. Don't underestimate the prowess of a panther, though. You've been warned.</em></sub>

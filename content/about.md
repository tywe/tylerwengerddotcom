+++
date  = "2016-11-19"
title = "About"
menu  = "main"
+++

## The Basics

My name's Tyler! Thanks for visiting.

## Personal

I currently live in Cincinnati, where small lizards rule with tiny iron fists. I love a good breakfast/crossword combo.

* Reading: Lapvona
* Listening: Hot Chocolate (Ooh, we got it)
* Watching: Cowboy Bebop
* Playing: Balatro
* And [here's a random Wikipedia page](http://en.wikipedia.org/wiki/Special:Random).

## Professional

I work for [DoiT](https://www.doit.com) as a Senior Cloud Architect. I have a degree in computer engineering from the University of Akron. If you've ever eaten a Whopper in Amish country, I may have made it.

## Contact

If you've found this website, then you're a capable person (also probably very smart and funny) and I'm sure you can find me elsewhere.

## Technical

This site is built with [Hugo](https://gohugo.io/) and Gitlab CI, and hosted on AWS. The full source code can be found [in this repo](https://gitlab.com/tywe/tylerwengerddotcom).

Other various projects include Twitter bots and single-page apps, mostly in Python and using cloud platforms like AWS or GCP. Everything's up on [Gitlab](https://gitlab.com/users/tywe/projects).

## What does "GmbH" mean?

[Gesellschaft mit beschränkter Haftung!](http://en.wikipedia.org/wiki/Gesellschaft_mit_beschr%C3%A4nkter_Haftung)

## Et Cetera

This site is run on the honor system. Play fair.
